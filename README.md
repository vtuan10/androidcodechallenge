# AndroidCodeChallenge
Code challenge for Android interviews @IconicFinance

## Exercise: Implement a login screen
![Login screen](./screens.png) 
The above screens are to be implemented in this challenge:
* The screen has 2 input fields Username and Password. The password should not be shown in cleartext when typing.
* A login button which reads the input fields and uses the data to login to the backend
* If the checkbox Remember me? is checked and Login is pressed:
    - Store the data “securely” on the client. You are free to decide how to implement this.
    - Username and Password are prefilled whenever the app is opened and stored data is available
* Show the JWT-Token in a dialog after successful login or an error message otherwise


## Exercise: Fix buggy code
[MainActivity](./app/src/main/java/de/heymoney/iconiccodechallenge/MainActivity.kt) has some functionality to calculate a secret character.
Unfortunately, It is some legacy code which was badly implemented. Please explain:
* What issues the code might have and come up with a fix
* How can one make the calculation faster?