

plugins {
    id("com.android.library")
    kotlin("android")
}

android {
    compileSdkVersion(30)
    buildToolsVersion = "30.0.3"

    defaultConfig {
        minSdkVersion(24)
        targetSdkVersion(30)
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {
    implementation(AndroidXDeps.core)
    implementation(AndroidXDeps.appCompat)
    implementation(AndroidXDeps.constraintLayout)
    implementation(MaterialDeps.material)

    implementation(Misc.koin)

    testImplementation(TestDeps.junit)
    androidTestImplementation(TestDeps.androidxTest)
    androidTestImplementation(TestDeps.espresso)
}