rootProject.name = "IconicCodeChallenge"

pluginManagement {
    repositories {
        gradlePluginPortal()
        google()
    }

    val agpVersion = "4.1.0"
    val kotlinVersion = "1.4.31"
    plugins {
        id("com.android.application") version agpVersion
        id("com.android.library") version agpVersion
        id("org.jetbrains.kotlin.android") version kotlinVersion
    }

    resolutionStrategy {
        eachPlugin {
            if (requested.id.id.startsWith("com.android")) {
                useModule("com.android.tools.build:gradle:${agpVersion}")
            }
        }
    }
}

include(
    ":app",
    ":networking",
    ":storage"
)
