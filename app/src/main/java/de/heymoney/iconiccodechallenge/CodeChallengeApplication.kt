package de.heymoney.iconiccodechallenge

import android.app.Application
import de.heymoney.networking.networkingModule
import de.heymoney.storage.storageModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class CodeChallengeApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@CodeChallengeApplication)
            modules(
                appModule,
                networkingModule,
                storageModule
            )
        }
    }
}