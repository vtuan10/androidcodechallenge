package de.heymoney.iconiccodechallenge

import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Exercise 3
        object : AsyncTask<Void?, Void?, String?>() {
            override fun doInBackground(vararg params: Void?): String? {
                return (1..5342345)
                    .map { (0..127).random() }
                    .map { it.toChar() }
                    .map { "The secret char is: $it" }
                    .first()
            }

            override fun onPostExecute(result: String?) {
                Toast.makeText(this@MainActivity, result, Toast.LENGTH_LONG).show()
            }
        }.execute()
    }
}