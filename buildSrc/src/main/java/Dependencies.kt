
object Version {
    val androidxCoreVersion = "1.3.2"
    val appCompatVersion = "1.2.0"
    val constraintLayoutVersion = "2.0.4"
    val materialVersion = "1.3.0"
    val junitVersion = "4.13.2"
    val androidxTestVersion = "1.1.2"
    val espressoVersion = "3.3.0"
    val koinVersion = "3.0.1-beta-1"
}

object MaterialDeps {
    val material = "com.google.android.material:material:${Version.materialVersion}"
}

object AndroidXDeps {
    val core = "androidx.core:core-ktx:${Version.androidxCoreVersion}"
    val appCompat = "androidx.appcompat:appcompat:${Version.appCompatVersion}"
    val constraintLayout = "androidx.constraintlayout:constraintlayout:${Version.constraintLayoutVersion}"
}

object TestDeps {
    val junit = "junit:junit:${Version.junitVersion}"
    val androidxTest = "androidx.test.ext:junit:${Version.androidxTestVersion}"
    val espresso = "androidx.test.espresso:espresso-core:${Version.espressoVersion}"
}

object Misc {
    val koin = "io.insert-koin:koin-android:${Version.koinVersion}"
}